import '../styles/globals.css'

import Link from 'next/link';

function marketPlace({Component, pageProps}) {
  return (
    <div>
      <nav className='border-b p-6' style={{backgroundColor: 'purple'}}>
        <p className='text-4x1'>Marketplace</p>
        <div className='flex mt-4 justify-center'>
          <Link href='/'>
            <a className='mr-4'>
              Marketplace
            </a>
          </Link>
          <Link href='/mint-item'>
            <a className='mr-6'>
              Mint tokens
            </a>
          </Link>
          <Link href='/bought-nfts'>
            <a className='mr-6'>
              Bought NFTs
            </a>
          </Link>
          <Link href='/sold-nfts'>
            <a className='mr-6'>
              Sold nfts
            </a>
          </Link>
        </div>
      </nav>
      <Component {...pageProps}></Component>
    </div>
  )
}

export default marketPlace;
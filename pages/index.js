import {ethers} from 'ethers'
import {useEffect, useState} from 'react'
import axios from 'axios'
import Web3Modal from 'web3modal'

import { nftaddress, marketaddress } from '../config';

import NFT from '../artifacts/contracts/NFT.sol/NFT.json';
import Martkeplace from '../artifacts/contracts/Marketplace.sol/Marketplace.json';

export default function Home() {

  const [nfts, setNfts] = useState([])
  const [loadingState, setLoadingState] = useState('not-loaded')

  useEffect(() => {
    loadNfts()
  }, [])

  async function loadNfts() {
    const provider = new ethers.providers.JsonRpcProvider();
    const tokenContract = new ethers.Contract(nftaddress, NFT.abi, provider);
    const marketContract = new ethers.Contract(marketaddress, Martkeplace.abi, provider);
    const data = await marketContract.fetchMarketTokens()

    let items = await Promise.all(data.map(async i => {
      const tokenUri = await tokenContract.tokenURI(i.tokenId)
      const meta = await axios({
        url: tokenUri,
        method: 'get'
      })
      let price = ethers.utils.formatUnits(i.price.toString(), 'ether')
      return {
        price,
        tokenId: i.tokenId.toNumber(),
        seller: i.seller,
        owner: i.owner,
        image: meta.data.image,
        name: meta.data.name,
        description: meta.data.description
      }
    }))

    setNfts(items)
    setLoadingState('loaded')
  }

  async function buyNFT(nft) {
    const web3modal = new Web3Modal()
    const connection = await web3modal.connect()
    const provider = new ethers.providers.Web3Provider(connection)
    const signer = provider.getSigner()
    const contract = new ethers.Contract(marketaddress, Martkeplace.abi, signer)

    const price = ethers.utils.parseUnits(nft.price.toString(), 'ether')
    const transaction = await contract.sale(nftaddress, nft.tokenId, {
      value: price
    })
    transaction.wait()
    loadNfts()
  }

  if (loadingState === 'loaded' && !nfts.length) return (
    <h1>No NFTs or whatsoever</h1>
  )

  return (
    <div className='flex justify-center'>
          {
            nfts.map((nft,i) => (
              <div key={i} className='px-4' style={{maxWidth: '160px'}}>
                <div className='grid grid-cols-1'>
                  <div className='border shadow rounded-x1 overflow-hidden'>
                    <img src={nft.image} />
                    <div className='p-4'>
                      <h3>{nft.name}</h3>
                      <p>{nft.description}</p>
                      <p>{nft.price} ETH</p>
                    </div>
                    <button
                      onClick={() => buyNFT(nft)}
                      className='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'
                    >
                      Buy
                    </button>
                  </div>
                </div>
              </div>
          ))
        }
    </div>
  )
}

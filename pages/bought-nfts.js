import {useEffect, useState} from 'react'
import { marketaddress, nftaddress } from '../config';
import { ethers } from 'ethers';
import Martkeplace from '../artifacts/contracts/Marketplace.sol/Marketplace.json';
import NFT from '../artifacts/contracts/NFT.sol/NFT.json';
import axios from 'axios';
import Web3Modal from 'web3modal'

export default function boughtNFTs() {

  const [tokens, setTokens] = useState([])
  const [loadingState, setLoadingState] = useState('not-loaded')

  useEffect(() => {
    loadMyNFTs()
  }, [])

  async function loadMyNFTs() {
    const web3 = new Web3Modal();
    const connection = await web3.connect()
    const provider = new ethers.providers.Web3Provider(connection)
    const signer = provider.getSigner()

    const marketContract = new ethers.Contract(marketaddress, Martkeplace.abi, signer)
    const nftContract = new ethers.Contract(nftaddress, NFT.abi, signer)
    const data = await marketContract.fetchMyOwnedNFTs()

    let items = await Promise.all(data.map(async item => {
      const tokenUri = await nftContract.tokenURI(item.tokenId)
      const meta = await axios({
        url: tokenUri,
        method: 'get'
      })
      return {
        name: meta.data.name,
        description: meta.data.description,
        image: meta.data.image
      }
    }))

    setTokens(items)
    setLoadingState('loaded')
  }

  if (loadingState != 'loaded') return (
    <h1>Loading...</h1>
  )

  if (loadingState === 'loaded' && !tokens.length) return (
    <h1>No NFTs or whatsoever</h1>
  )

  return (
    <div className='flex justify-center'>
      {
        tokens.map((nft,i) => (
          <div key={i} className='px-4' style={{maxWidth: '160px'}}>
            <div className='grid grid-cols-1'>
              <div className='border shadow rounded-x1 overflow-hidden'>
                <img src={nft.image} />
                <div className='p-4'>
                  <h3>{nft.name}</h3>
                  <p>{nft.description}</p>
                </div>
              </div>
            </div>
          </div>
        ))
      }
    </div>
  )
}
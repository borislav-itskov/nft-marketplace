import {ethers} from 'ethers'
import {useState} from 'react'
import Web3Modal from 'web3modal'

import { nftaddress, marketaddress } from '../config';
import { create as ipfsHttpClient } from 'ipfs-http-client';

import NFT from '../artifacts/contracts/NFT.sol/NFT.json';
import Martkeplace from '../artifacts/contracts/Marketplace.sol/Marketplace.json';
import { useRouter } from 'next/router';

const infuraUrl = 'https://ipfs.infura.io:5001/api/v0'
const infuraIpfsUrl = 'https://ipfs.infura.io/ipfs'
const client = ipfsHttpClient(infuraUrl)

export default function MintItem() {

  const [fileUrl, setFileUrl] = useState(null)
  const [formInput, updateFormInput] = useState({price: '', name: '', description: ''})
  const router = useRouter()

  async function onChange(e) {
    const file = e.target.files[0]

    try {
      const added = await client.add(file, {
        progress: (prog) => console.log(`received ${prog}`)
      })
      const url = infuraIpfsUrl + '/' + added.path
      setFileUrl(url)
    } catch (error) {
      console.log(error)
    }
  }

  async function putInOnTheMarket() {
    const {name, description, price} = formInput;
    if (!name || !description || !price || !fileUrl) return;

    const data = JSON.stringify({
      name, description, image: fileUrl
    })

    try {
      const added = await client.add(data);
      const url = infuraIpfsUrl + '/' + added.path
      createSale(url)
    } catch (error) {
      console.log(error)
    }
  }

  async function createSale(url) {
    const web3modal = new Web3Modal()
    const connection = await web3modal.connect()
    const provider = new ethers.providers.Web3Provider(connection)
    const signer = provider.getSigner()
    const nftContract = new ethers.Contract(nftaddress, NFT.abi, signer)

    const transaction = await nftContract.mintToken(url);
    const tx = await transaction.wait()
    let event = tx.events[0]
    let value = event.args[2]
    let tokenId = value.toNumber()
    const price = ethers.utils.parseUnits(formInput.price, 'ether')

    // list the item for sale
    const marketContract = new ethers.Contract(marketaddress, Martkeplace.abi, signer);
    const listingPrice = await marketContract.getListingPrice();
    listingPrice = listingPrice.toString();

    transaction = await marketContract.createMarketItem(nftaddress, tokenId, price, {
      value: listingPrice
    })
    transaction.wait()
    router.push('./')
  }

  return (
    <div className='flex justify-center'>
      <div>
        <label
          className="block text-gray-700 text-sm font-bold mb-2"
          htmlFor="name"
        >
          Name
        </label>
        <input
          className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
          id="name"
          type="text"
          onChange={ e => updateFormInput({...formInput, name: e.target.value})}
          placeholder='Name'
        />
        <label
          className="block text-gray-700 text-sm font-bold mb-2"
          htmlFor="description"
        >
          Description
        </label>
        <textarea
          className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
          onChange={ e => updateFormInput({...formInput, description: e.target.value})}
          placeholder='Description'
          id='description'
        ></textarea>
        <label
          className="block text-gray-700 text-sm font-bold mb-2"
          htmlFor="price"
        >
          Price
        </label>
        <input
          onChange={ e => updateFormInput({...formInput, price: e.target.value})}
          placeholder='Price in ETH'
          id='price'
          className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
        />
        <input
          type='file'
          placeholder='File'
          className='mt-4 border rounded'
          onChange={onChange}
          placeholder='Price in ETH'
        />
        {
          fileUrl && (
            <img src={fileUrl} width='350px' className='rounded mt-4' />
          )
        }
        <div className='mt-4'>
          <button
            className='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'
            onClick={putInOnTheMarket}
          >
            Mint NFT
          </button>
        </div>
      </div>
    </div>
  )
}
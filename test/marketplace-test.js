const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("Marketplace", function () {
  it("Should mint and trade NFTs", async function () {
    const Marketplace = await ethers.getContractFactory("Marketplace");
    const marketplace = await Marketplace.deploy();
    await marketplace.deployed();
    const marketAddress = marketplace.address;

    const NFT = await ethers.getContractFactory("NFT");
    const nft = await NFT.deploy(marketAddress);
    await nft.deployed();
    const nftAddress = nft.address;

    let listingPrice = await marketplace.getListingPrice()
    expect(ethers.utils.formatUnits(listingPrice)).to.equal("0.045");

    let first_token = await nft.mintToken('http://token-1');
    let second_token = await nft.mintToken('http://token-2');

    let auctionPrice = ethers.utils.parseEther("10");
    let first_token_placed = await marketplace.createMarketItem(nftAddress, 1, auctionPrice, {
      value: listingPrice
    });
    let second_token_placed = await marketplace.createMarketItem(nftAddress, 2, auctionPrice, {
      value: listingPrice
    });

    const [_, buyer] = await ethers.getSigners();
    let buy_nft = await marketplace.connect(buyer).sale(nftAddress, 1, {
      value: auctionPrice
    })

    let items = await marketplace.fetchMarketTokens();
    expect(items).to.not.be.empty;

    items = await Promise.all(items.map(async i => {
      const tokenUri = await nft.tokenURI(i.tokenId)
      expect(ethers.utils.formatUnits(i.price)).to.equal("10.0");

      return {
        price: ethers.utils.formatUnits(i.price),
        tokenId: i.tokenId.toString(),
        seller: i.seller,
        owner: i.owner,
        tokenUri
      }
    }))
  });
});

//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "hardhat/console.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/token/ERC721/utils/ERC721Holder.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract Marketplace is ReentrancyGuard, Ownable, ERC721Holder {
    Counters.Counter private marketItemIds;
    Counters.Counter private tokenSold;

    uint public listingPrice = 0.045 ether;

    struct MarketToken {
        uint itemId;
        address nftContract;
        uint tokenId;
        address seller;
        address owner;
        uint price;
        bool sold;
    }

    mapping(uint => MarketToken) private idToMarketToken;

    event MarketTokenMinted(
        uint indexed itemId,
        address indexed nftContract,
        uint indexed tokenId,
        address seller,
        address owner,
        uint price,
        bool sold
    );

    function getListingPrice() public view returns (uint256) {
        return listingPrice;
    }

    function createMarketItem(address nftContract, uint tokenId, uint price)
        public payable nonReentrant {

        require(price > 0, "Price must be bigger");
        require(msg.value == listingPrice, "listingPrice mismatch");

        Counters.increment(marketItemIds);
        uint marketItemId = Counters.current(marketItemIds);

        idToMarketToken[marketItemId] = MarketToken(
            marketItemId,
            nftContract,
            tokenId,
            address(msg.sender),
            address(0),
            price,
            false
        );

        // nft transaction
        IERC721(nftContract).safeTransferFrom(msg.sender, payable(address(this)), tokenId);

        emit MarketTokenMinted(
            marketItemId,
            nftContract,
            tokenId,
            address(msg.sender),
            address(0),
            price,
            false
        );
    }

    // function for sales
    function sale(address nftContract, uint marketItemId)
        public payable nonReentrant {
        uint price = idToMarketToken[marketItemId].price;
        uint tokenId = idToMarketToken[marketItemId].tokenId;
        require(msg.value == price, 'Not enough cash');

        // transfer the amount to the seller
        payable(idToMarketToken[marketItemId].seller).transfer(msg.value);

        // transfer the NTF to the buyer
        IERC721(nftContract).safeTransferFrom(address(this), payable(address(msg.sender)), tokenId);
        idToMarketToken[marketItemId].owner = msg.sender;
        idToMarketToken[marketItemId].sold = true;
        Counters.increment(tokenSold);
    }

    function fetchMarketTokens() public view returns(MarketToken[] memory) {
        uint itemCount = Counters.current(marketItemIds);
        uint soldCount = Counters.current(tokenSold);
        uint leftCount = itemCount - soldCount;
        uint currentIndex = 0;

        MarketToken[] memory items = new MarketToken[](leftCount);

        for (uint256 index = 1; index <= itemCount; index++) {
            MarketToken memory marketToken = idToMarketToken[index];
            if (marketToken.sold) continue;

            items[currentIndex] = marketToken;
            currentIndex++;
        }

        return items;
    }

    function fetchMyOwnedNFTs() public view returns(MarketToken[] memory) {
        uint itemCount = Counters.current(marketItemIds);
        uint userItemCount = 0;
        uint currentIndex = 0;

        for (uint256 index = 1; index <= itemCount; index++) {
            MarketToken memory marketToken = idToMarketToken[index];
            if (marketToken.owner != address(msg.sender)) continue;

            userItemCount++;
        }

        MarketToken[] memory items = new MarketToken[](userItemCount);

        for (uint256 index = 1; index <= itemCount; index++) {
            MarketToken memory marketToken = idToMarketToken[index];
            if (marketToken.owner != address(msg.sender)) continue;

            items[currentIndex] = marketToken;
            currentIndex++;
        }

        return items;
    }

    function fetchMySoldNtfs() public view returns(MarketToken[] memory) {
        uint itemCount = Counters.current(marketItemIds);
        uint userItemCount = 0;
        uint currentIndex = 0;

        // get the number of items sold
        for (uint256 index = 1; index <= itemCount; index++) {
            MarketToken memory marketToken = idToMarketToken[index];
            if (
                marketToken.seller != address(msg.sender)
                || !marketToken.sold
            ) continue;

            userItemCount++;
        }

        MarketToken[] memory items = new MarketToken[](userItemCount);

        for (uint256 index = 1; index <= itemCount; index++) {
            MarketToken memory marketToken = idToMarketToken[index];
            if (
                marketToken.seller != address(msg.sender)
                || !marketToken.sold
            ) continue;

            items[currentIndex] = marketToken;
            currentIndex++;
        }

        return items;
    }
}

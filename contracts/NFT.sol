//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "hardhat/console.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

contract NFT is ERC721URIStorage {

    /** The store where NTFs are going to be exchanged */
    address private marketAddress;
    Counters.Counter private tokenIds;

    constructor(address _marketAddress) ERC721("nft_bird", "NFTBIRD") {
        marketAddress = _marketAddress;
    }

    function mintToken(string memory tokenURI) public returns(uint) {
        Counters.increment(tokenIds);
        uint tokenId = Counters.current(tokenIds);

        // create a token for the sender
        _mint(msg.sender, tokenId);

        // allow the marketplace to trade tokens
        setApprovalForAll(marketAddress, true);

        // set the token URI
        _setTokenURI(tokenId, tokenURI);

        return tokenId;
    }
}

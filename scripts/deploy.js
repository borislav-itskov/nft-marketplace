const hre = require("hardhat");
const fs = require('fs');

async function main() {

  // We get the contract to deploy
  const Marketplace = await hre.ethers.getContractFactory("Marketplace");
  const marketplace = await Marketplace.deploy();
  await marketplace.deployed();
  const marketAddress = marketplace.address;

  const NFT = await hre.ethers.getContractFactory("NFT");
  const nft = await NFT.deploy(marketAddress);
  await nft.deployed();
  const nftAddress = nft.address;

  let config = `export const marketaddress= '${marketAddress}'
export const nftaddress= '${nftAddress}'`

  let data = JSON.stringify(config);
  fs.writeFileSync('config.js', JSON.parse(data));
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
